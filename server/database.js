var Sequelize = require("sequelize");
var config = require('./config');
console.log(config);
console.log(config.db.dialet);
var database = new Sequelize(
    config.db.database,
    config.db.username,

    config.db.password, {
        host: config.db.host,
        dialect: config.db.dialet,
        pool:{
            max: 5,
            min: 0,
            idle:10000
        },
        force:false
    });

var GroceryModel = require('./models/grocery.model')(database);

database.sync({force: config.db.sync})
    .then(function () {
        console.log("DB in sync")
    });

module.exports = {
    Grocery: GroceryModel
};

