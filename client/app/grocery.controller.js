(function () {
    angular.module("MyGroceryApp")
        .controller("ListCtrl", ListCtrl)
        .controller("AddCtrl", AddCtrl)
        .controller("EditCtrl", EditCtrl);

    ListCtrl.$inject = ['GroceryService', '$state'];

    function ListCtrl(GroceryService, $state) {
        var vm = this;
        vm.products = "";
        vm.typesOfSearch = ['Brand','Name'];
        vm.searchType = [];
        vm.searchType.selectedType = [];
        vm.sortBy = "";
        vm.keyword = "";

        vm.totalItems = 0;
        vm.itemsPerPage = 20;
        vm.currentPage = 1;
        vm.maxSize = 5; // control this number of display items on the pagination.

        vm.pageChanged = function() {
            console.log('Page changed to: ' + vm.currentPage);

            GroceryService.search(vm.searchType, vm.keyword, vm.sortBy, vm.itemsPerPage, vm.currentPage)
                .then(function (products) {
                    vm.products = products.rows;
                    vm.totalItems = products.count;
                }).catch(function (err) {
                console.info("Some Error Occurred",err);
                growl.error("Some Error Occurred");  
            });
        };

        vm.search = function (searchType, keyword, sortBy) {
            console.log(searchType.length);
            console.log(searchType);
            if( searchType.length==0) {
                alert('Please select at least one search type');
            }
            else {
                vm.searchType = searchType;
                vm.keyword = keyword;
                GroceryService.search(searchType, keyword, sortBy, vm.itemsPerPage, vm.currentPage)
                    .then(function (products) {
                        console.log(products);
                        vm.products = products.rows;
                        vm.totalItems = products.count;
                    })
                    .catch(function (err) {
                        console.info("Some Error Occurred",err);
                        growl.error("Some Error Occurred"); 
                    });
            }
        };
        
        vm.getProduct = function (id) {
            $state.go("edit", {'productId' : id});
        };

        GroceryService.search(vm.searchType, vm.keyword, vm.sortBy, vm.itemsPerPage, vm.currentPage)
            .then(function (products) {
                console.log("----- refresh list ");
                vm.products = products.rows;
                vm.totalItems = products.count;
            }).catch(function (err) {
            console.info("Some Error Occurred",err);
            growl.error("Some Error Occurred"); 
        });

    }

    EditCtrl.$inject = ['GroceryService', '$stateParams', '$state', 'growl'];

    function EditCtrl(GroceryService, $stateParams, $state, growl) {
        var vm = this;
        vm.product = {};

        vm.cancel = function () {
            $state.go("list");
        };
        
        GroceryService.edit($stateParams.productId)
            .then(function (product) {
                console.log(product);
                vm.product = product;
            }).catch(function (err) {
                console.info("Some Error Occurred",err);
                growl.error("Some Error Occurred"); 
            });

        vm.save = function () {
            console.log("Saving the changes");
            GroceryService.save(vm.product)
                .then(function (result) {
                    console.info("Product saved." + JSON.stringify(result));
                    growl.success(`Product saved. ${vm.product.name}`); 
                    $state.go("list");
                }).catch(function (err) {
                console.info("Some Error Occurred",err);
                growl.error("Some Error Occurred"); 
            });
        }

        vm.remove = function () {
            console.log("Removing this product");
            if (confirm("Do you really want to remove this product from your Groceries?") == true) {
                GroceryService.remove(vm.product)
                    .then(function (result) {
                        console.info("Product removed." + result);
                        growl.success(`Product removed. ${vm.product.name}`); 
                        $state.go("list");
                    }).catch(function (err) {
                    console.info("Some Error Occurred",err);
                    growl.error("Some Error Occurred"); 
                });
            } else {
                //do nothing
            }
        }
    }

    AddCtrl.$inject = ['GroceryService', '$stateParams', '$state', 'growl'];
    
    function AddCtrl(GroceryService, $stateParams, $state, growl) {
        var vm = this;
        vm.product = {};
        vm.UPC12Exist = false;
        vm.cancel = function () {
            $state.go("list");
        };
        
        vm.save = function () {
            console.log("Saving the changes");
            GroceryService.isUPC12Exist(vm.product)
            .then(function (result) {
                console.info("Is UPC12 exist ?" + JSON.stringify(result));
                console.info("Is UPC12 exist ?" + result.data.count);
                if(result.data.count == 0){
                    console.info("Is UPC12 exist ?" + result.data.count);
                    GroceryService.add(vm.product)
                    .then(function (result) {
                        console.info("Product saved." + JSON.stringify(result));
                        growl.success(`Product saved. ${vm.product.name}`); 
                        $state.go("list");
                    }).catch(function (err) {
                        console.info("Some Error Occurred",err);
                        growl.error("Some Error Occurred"); 
                    });
                }else{
                    vm.UPC12Exist = true;
                }
            }).catch(function (err) {
                console.info("Some Error Occurred",err);
                growl.error("Some Error Occurred"); 
            });
        }
    }
})();